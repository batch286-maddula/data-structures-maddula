numbers = [1, 2, 3, 4, 5]
print(f'list of numbers: {numbers}')

meals = ['spam', 'Spam', 'SPAM!']
print(f'list of numbers: {meals}')

message_list= ['Hi!']
message_list = message_list *4
print(f'Result of repeat operator: {message_list}')
print(f"List literals and operations using 'numbers' list:")
print(f'Length of the list: {len(numbers)}')
print(f'The last item was removed from list: {numbers.pop()}')

reversed_list = numbers[::-1]
print(f'The list in reversed order: {reversed_list}')
print(f'using indexing operation:{meals[2]}')
print(f'using negative indexing operation: {meals[-2]}')

meals[meals.index("Spam")] = "eggs"
print(f'Modified list: {meals}')

adding_item = ["bacon"]
Added_list= meals+ adding_item
print(f'Added a new item int he list: {Added_list}')

Added_list.sort()
print(f'Modified list after sort method: {Added_list}')
